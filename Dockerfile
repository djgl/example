FROM centos:7

RUN yum -y groupinstall "Development Tools" \
    && yum -y install openssl-devel bzip2-devel libffi-devel wget python3 \
    && wget https://www.python.org/ftp/python/3.9.0/Python-3.9.0.tgz \
    && tar xvf Python-3.9.0.tgz \
    && cd Python-3.9.0 \
    && ./configure --enable-optimizations \
    && make altinstall \
    && rm -rf ../Python-3.9.0 ../Python-3.9.0.tgz \
    && yum clean all

RUN pip3 install flask flask-jsonpify flask-restful

ADD python-api.py /python-api/
ENTRYPOINT [ "python3", "/python-api/python-api.py" ]
